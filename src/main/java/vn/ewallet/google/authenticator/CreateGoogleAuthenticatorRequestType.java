package vn.ewallet.google.authenticator;

import vn.mog.airtime.contract.base.MobiliserRequestType;

public class CreateGoogleAuthenticatorRequestType extends MobiliserRequestType {
	protected Long customerId;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

}
