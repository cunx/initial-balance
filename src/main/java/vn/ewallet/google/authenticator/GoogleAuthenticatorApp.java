package vn.ewallet.google.authenticator;

import java.util.Arrays;
import java.util.List;

import vn.ewallet.init.balance.AirtimeAPIClient;
import vn.ewallet.init.balance.AuthServiceClient;
import vn.ewallet.init.balance.SharedConstants;

public class GoogleAuthenticatorApp {

	public static List<Integer> ids = Arrays.asList(77, 80, 106, 109, 112, 115, 118, 128, 131, 134, 145, 148, 151, 154,
			161, 164, 169, 172, 175, 178, 182, 185, 188, 191, 198, 205, 209, 213, 216, 220, 251, 259, 827, 865, 869,
			985, 989, 998, 1002, 1011, 1016, 1033, 1043, 1047, 1059, 1088, 1100, 668, 1007, 1067, 1071, 1076, 1080);

/*	public static void main(String[] args) throws Exception {
		start();

	}*/

	private static void start() throws Exception {
		String requestURI = "http://172.30.2.11:11230/" + "/api/v1/customer/googleAutheticator/generate";
		AirtimeAPIClient client = new AirtimeAPIClient(authenticate());
		for (Integer id : ids) {
			CreateGoogleAuthenticatorRequest request = new CreateGoogleAuthenticatorRequest();
			request.setCustomerId(id.longValue());
			CreateGoogleAuthenticatorResponse response = client.callRequest(requestURI, request,
					CreateGoogleAuthenticatorResponse.class);
			System.out.println("--> "+id);
		}

	}

	private static String authenticate() {
		AuthServiceClient authClient = new AuthServiceClient();
		String accessToken = authClient.getAccessToken();
		System.out.println("accessToken: " + accessToken);
		return accessToken;
	}

}
