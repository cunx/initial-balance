package vn.ewallet.init.balance;

import org.springframework.stereotype.Service;

@Service
public class SharedConstants {

	// ---- Base Payment Url ------------------

	//public static String WALLET_PAYMENT_BASE_REQUEST_URL = "http://172.32.1.18:11200/airtime";
	public static String WALLET_PAYMENT_BASE_REQUEST_URL = "http://127.0.0.1:11130/airtime";

	// ---- Base Service Url ------------------
	public static String WALLET_SERVICE_BASE_REQUEST_URL = "http://127.0.0.1:11110/";

	public static int HTTP_CLIENT_CONNECT_TIMEOUT = 5000;// ms
	public static int HTTP_CLIENT_SOCKET_TIMEOUT = 20000;// ms

	// ------ Authentication ----------------------
//	public static String AUTH_SERVER_BASE_URL = "http://172.32.1.18:11230/";
	public static String AUTH_SERVER_BASE_URL = "http://127.0.0.1:11120/";
	public static String AUTH_SERVER_CLIENT_ID = "clientapp";
	public static String AUTH_SERVER_CLIENT_SECRET = "123456";
	public static String AUTH_USERNAME_CRON_TASK = "admin@truemoney.com.vn";
	public static String AUTH_PASSWORD_CRON_TASK = "Password@123";
	public static String AUTH_USERNAME_TYPE_CRON_TASK = "5";
	public static String AUTH_PASSWORD_TYPE_CRON_TASK = "1";

	// ------------- balance --------------------

	public static Long SOF_INITIAL_BALANCE = 500000000000L;

	public static String POOL_NAME = "actual_system_account@truemoney.com.vn";

	public static String CASH_ON_HAND_SOF_NAME = "tm_sof_cash_on_hand@truemoney.com.vn";

	public static String FILE_BASE_DIR = "/tmp/payment/";

	public static String SOF_FILE = "sof.xlsx";

	public static String CUSTOMER_FILE = "customer.xlsx";

}
