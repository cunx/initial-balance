package vn.ewallet.init.balance;

import java.io.File;

public class BalanceInitialApplication {
	public static void main(String[] args) throws Exception {
		System.out.println("========= START ==========");
		System.out.println("\n\n\n1. Test authentication.\n2. Init Sof balance\n3. Init common customer balance\n");
		if (args.length < 1) {
			System.out.println("No argument passed.Exit");
			return;
		}

		try {
			int choice = Integer.valueOf(args[0]);
			//int choice = 2;
			switch (choice) {
			case 1:
				System.out.println("\n========> Test athentication.");
				authenticate();

				break;
			case 2:
				System.out.println("\n========> Init Sof balance.");
				initSOFBalance();
				break;
			case 3:
				System.out.println("\n========> Init Common customer balance.");
				initCustomerBalance();
				break;

			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("========= END ============");
	}

	private static String authenticate() {
		AuthServiceClient authClient = new AuthServiceClient();
		String accessToken = authClient.getAccessToken();
		System.out.println("accessToken: " + accessToken);
		return accessToken;
	}

	private static void initCustomerBalance() throws Exception {
		String accessToken = authenticate();
		CustomerBalanceInit s = new CustomerBalanceInit(accessToken);
		String customerFileName = SharedConstants.FILE_BASE_DIR + File.separator + SharedConstants.CUSTOMER_FILE;
		System.out.println("----> Starting initial balance customer ...");
		s.initCustomerBalance(SharedConstants.CASH_ON_HAND_SOF_NAME, customerFileName);
		System.out.println("----> Finish initial balance customer");

	}

	private static void initSOFBalance() throws Exception {
		String accessToken = authenticate();
		CustomerBalanceInit s = new CustomerBalanceInit(accessToken);
		String sofFileName = SharedConstants.FILE_BASE_DIR + File.separator + SharedConstants.SOF_FILE;
		System.out.println("---> Starting initial balance sof ...");
		s.initSofBalance(SharedConstants.POOL_NAME, sofFileName);
		System.out.println("---> Finish initial balance sof");
	}

}
