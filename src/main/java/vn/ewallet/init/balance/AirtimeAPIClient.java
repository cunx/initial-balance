package vn.ewallet.init.balance;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class AirtimeAPIClient {
	// --------------------------------
	public static final String AUTHORIZATION = "Authorization";
	public static final String BEARER_HEADER_PREFIX = "Bearer ";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String USER_AGENT = "User-Agent";
	public static final String authoriseUrl = SharedConstants.WALLET_PAYMENT_BASE_REQUEST_URL + "/api/authorise";
	public static final String getCustomerUrl = SharedConstants.WALLET_PAYMENT_BASE_REQUEST_URL + "/api/v1/customer/getCustomerByUserName";
	
	protected RestTemplate restTemplate = new RestTemplate();

	private String accessToken;
	

	public AirtimeAPIClient(String accessToken) {
		super();
		this.accessToken = accessToken;
	}
	
	public <T> T authorise(Object request, Class<T> clazz) throws Exception {
		return callRequest(authoriseUrl, request, clazz);
	}

	public <T> T getCustomers(Object request, Class<T> clazz) throws Exception {
		return callRequest(getCustomerUrl, request, clazz);
	}
	
	// --------------------------------
	public <T> T callRequest(String requestURI, Object request, Class<T> clazz) throws Exception {
		Map<String, String> mapHeader = new HashMap<String, String>();
		mapHeader.put(AUTHORIZATION, BEARER_HEADER_PREFIX + accessToken);
		return callRequest(requestURI, mapHeader, request, clazz);
	}

	public <T> T callRequest(String requestURI, Map<String, String> mapHeader, Object request, Class<T> clazz)
			throws Exception {
		try {
			final String url = requestURI;
			// --------------
			// --------------
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			if (mapHeader != null && !mapHeader.isEmpty()) {
				Iterator<Entry<String, String>> iterator = mapHeader.entrySet().iterator();
				while (iterator.hasNext()) {
					Entry<String, String> entry = iterator.next();
					headers.add(entry.getKey(), entry.getValue());
				}
			}
			final HttpEntity walletRequest = new HttpEntity(request, headers);
			// --------------
			T resp = restTemplate.postForObject(url, walletRequest, clazz);
			if (resp == null)
				throw new NullPointerException();
			return resp;
		} catch (HttpStatusCodeException ex) {
			int statusCode = ex.getStatusCode().value();
			if (statusCode == 401)
				throw new SecurityException("Not Authorized!");
			throw ex;
		} catch (RestClientException ex) {
			// Server not found
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
	}
}
