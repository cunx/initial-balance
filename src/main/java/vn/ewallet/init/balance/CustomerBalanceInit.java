package vn.ewallet.init.balance;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;

import vn.mog.airtime.contract.base.AuthorisationRequest;
import vn.mog.airtime.contract.base.AuthorisationResponse;
import vn.mog.airtime.contract.base.GetCustomerByUserNameRequest;
import vn.mog.airtime.contract.base.GetCustomerByUserNameResponse;

public class CustomerBalanceInit {
	private final static Logger logger = Logger.getLogger(CustomerBalanceInit.class);
	private String accessToken;

	public CustomerBalanceInit(String accessToken) {
		this.accessToken = accessToken;
	}

	public void initCustomerBalance(String sof, String fileName){
		logger.info("======= INIT CUSTOMER ===========");
		List<Customer> customerL = ExcelUtils.readCustomerExcelFile(fileName);
		AirtimeAPIClient airtimeClient = new AirtimeAPIClient(accessToken);
		Long sofId = null;
		try {
			List<Customer> sofL = Arrays.asList(new Customer(null, sof, null, null));
			getRealIdOnPayment(sofL, airtimeClient);
			sofId = sofL.get(0).getId();
			logger.info("===> SofName: "+sof+"===> SofId: "+sofId);
			getRealIdOnPayment(customerL,airtimeClient);
		} catch (Exception e1) {
			logger.info("=====> Get Real Info customer on payment fail.");
			return;
		}
		
		AuthorisationResponse response = null;
		AuthorisationRequest request = null;
		int totalSuccess = 0;
		int totalFail = 0;
		int total = 0;
		StringBuilder sb = new StringBuilder();
		for (Customer c : customerL) {
			total++;
			request = makeCustomerRequest(c, sofId);
			response = new AuthorisationResponse();
			logger.info("REQ: " + Utils.objectToJson(request));
			try {
				response = airtimeClient.authorise(request, AuthorisationResponse.class);
				if (response.getStatus().getCode() != 0) {
					throw new Exception();
				}
				totalSuccess++;
			} catch (Exception e) {
				totalFail++;
				sb.append(c.getEmail()).append(",");
			}
			logger.info("RESP: " + Utils.objectToJson(response));
		}
		logger.info("Total: " + total);
		logger.info("Total Success: " + totalSuccess);
		logger.info("Total Fail: " + totalFail);
		if(totalFail > 0) {
			logger.info("Detail account fail: "+sb.toString());
		}
		logger.info("======= END INIT CUSTOMER ===========");
	}

	private void getRealIdOnPayment(List<Customer> customerL, AirtimeAPIClient airtimeClient) throws Exception {
		GetCustomerByUserNameResponse response = new GetCustomerByUserNameResponse();
		GetCustomerByUserNameRequest request = null;
		for(Customer c : customerL) {
			request = new GetCustomerByUserNameRequest();
			request.setUserName(c.getEmail());
			response = airtimeClient.getCustomers(request, GetCustomerByUserNameResponse.class);
			if(response.getStatus().getCode() != 0 || response.getCustomer() == null)
				throw new Exception("get customer fail.");
			vn.mog.airtime.contract.base.Customer realCustomer = response.getCustomer();
			c.setId(realCustomer.getId());
		}
		
	}

	public void initSofBalance(String pool, String fileName) {
		logger.info("======= INIT SOF ===========");
		List<Customer> sofL = ExcelUtils.readCustomerExcelFile(fileName);
		AirtimeAPIClient airtimeClient = new AirtimeAPIClient(accessToken);
		Long poolId = null;
		try {
			List<Customer> poolL = Arrays.asList(new Customer(null, pool, null, null));
			getRealIdOnPayment(poolL, airtimeClient);
			poolId = poolL.get(0).getId();
			logger.info("===> PoolName: "+pool+"===> PoolId: "+poolId);
			getRealIdOnPayment(sofL,airtimeClient);
		} catch (Exception e1) {
			logger.info("=====> Get Real Info customer on payment fail.");
			return;
		}
		
		AuthorisationResponse response = null;
		AuthorisationRequest request = null;
		int totalSuccess = 0;
		int totalFail = 0;
		int total = 0;
		StringBuilder sb = new StringBuilder();
		for (Customer c : sofL) {
			total++;
			request = makeSofRequest(c, poolId);
			response = new AuthorisationResponse();
			logger.info("REQ: " + Utils.objectToJson(request));
			try {
				response = airtimeClient.authorise(request, AuthorisationResponse.class);
				if (response.getStatus().getCode() != 0) {
					throw new Exception();
				}
				totalSuccess++;
			} catch (Exception e) {
				totalFail++;
				sb.append(c.getEmail()).append(",");
			}
			logger.info("RESP: " + Utils.objectToJson(response));
		}
		logger.info("Total: " + total);
		logger.info("Total Success: " + totalSuccess);
		logger.info("Total Fail: " + totalFail);
		if(totalFail > 0) {
			logger.info("Detail account fail: "+sb.toString());
		}
		logger.info("======= END INIT SOF ===========");
	}

	private AuthorisationRequest makeCustomerRequest(Customer c, Long sofId) {
		AuthorisationRequest request = new AuthorisationRequest();
		request.setOrderId(UUID.randomUUID().toString().replaceAll("-", ""));

		request.setAmount(c.getBalance());

		request.setAutoCapture(true);
		request.setContent("Fund in for customer.");
		request.setOrderChannel("WALLET_FUND_IN");
		request.setOrigin("authorise");
		request.setPayeeId(c.getId());
		request.setPayerId(sofId);
		request.setTraceNo(UUID.randomUUID().toString());
		request.setUsecase(2);
		return request;
	}

	private AuthorisationRequest makeSofRequest(Customer c, Long poolId) {
		Long balance = c.getBalance();
		AuthorisationRequest request = new AuthorisationRequest();
		request.setOrderId(UUID.randomUUID().toString().replaceAll("-", ""));
		request.setAmount(balance);
		request.setAutoCapture(true);
		request.setContent("Init balance for sof.");
		request.setOrderChannel("PAYMENT");
		request.setOrigin("authorise");
		request.setPayeeId(c.getId());
		request.setPayerId(poolId);
		request.setTraceNo(UUID.randomUUID().toString());
		request.setUsecase(2);
		return request;
	}

}
