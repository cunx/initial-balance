package vn.ewallet.init.balance;

public class Customer {
	private Long id;

	private String email;
	private String customerType;
	private Long balance;

	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Customer(Long id, String email, String customerType, Long balance) {
		super();
		this.id = id;
		this.email = email;
		this.customerType = customerType;
		this.balance = balance;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public String getCustomerType() {
		return customerType;
	}

	public Long getBalance() {
		return balance;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public void setBalance(Long balance) {
		this.balance = balance;
	}

}
