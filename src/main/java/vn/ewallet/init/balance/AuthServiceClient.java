package vn.ewallet.init.balance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AuthServiceClient {

	private static MultiThreadedHttpConnectionManager httpConnectionManager;
	private static HttpClient httpClient;
	static {

		if (httpConnectionManager == null) {
			httpConnectionManager = new MultiThreadedHttpConnectionManager();
			HttpConnectionManagerParams params = new HttpConnectionManagerParams();
			params.setDefaultMaxConnectionsPerHost(100);
			params.setMaxTotalConnections(5000);
			params.setParameter(HttpConnectionManagerParams.CONNECTION_TIMEOUT,
					SharedConstants.HTTP_CLIENT_CONNECT_TIMEOUT);
			params.setParameter(HttpConnectionManagerParams.SO_TIMEOUT, SharedConstants.HTTP_CLIENT_SOCKET_TIMEOUT);
			httpConnectionManager.setParams(params);
		}
		if (httpClient == null)
			httpClient = new HttpClient(httpConnectionManager);
	}

	public String getAccessToken() {
		final String url = SharedConstants.AUTH_SERVER_BASE_URL + "/api/v1/auth/token";
		String accessToke = null;
		// Param
		final String grantType = "password";
		final String scope = "read write";
		final String clientSecret = SharedConstants.AUTH_SERVER_CLIENT_SECRET; // "123456";
		final String clientId = SharedConstants.AUTH_SERVER_CLIENT_ID; // "clientapp";
		final String basicAuthen = clientId + ":" + clientSecret; // "clientapp:123456";
		final String identification = SharedConstants.AUTH_USERNAME_CRON_TASK;
		final String credential = SharedConstants.AUTH_PASSWORD_CRON_TASK;
		final String identifierType = SharedConstants.AUTH_USERNAME_TYPE_CRON_TASK;
		final String credentialType = SharedConstants.AUTH_PASSWORD_TYPE_CRON_TASK;

		// request.
		// -----------
		final PostMethod method = new PostMethod(url);
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new NameValuePair("username", identification));
			urlParameters.add(new NameValuePair("password", credential));
			urlParameters.add(new NameValuePair("grant_type", grantType));
			urlParameters.add(new NameValuePair("scope", scope));
			urlParameters.add(new NameValuePair("client_secret", clientSecret));
			urlParameters.add(new NameValuePair("client_id", clientId));
			urlParameters.add(new NameValuePair("identifierType", String.valueOf(identifierType)));
			urlParameters.add(new NameValuePair("credentialType", String.valueOf(credentialType)));

			method.addParameters(urlParameters.toArray(new NameValuePair[urlParameters.size()]));

			String encoding = DatatypeConverter.printBase64Binary(basicAuthen.getBytes("UTF-8"));
			method.setRequestHeader("Authorization", "Basic " + encoding);

			httpClient.executeMethod(method);

			String json = method.getResponseBodyAsString();
			if (json != null) {
				AuthToken authToken = new ObjectMapper().readValue(json, AuthToken.class);
				accessToke = authToken.getAccessToken();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			method.releaseConnection();
		}
		return accessToke;
	}
}
