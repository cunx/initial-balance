package vn.ewallet.init.balance;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {

	public static List<Customer> readCustomerExcelFile(String fileName) {
		List<Customer> customerL = new ArrayList<Customer>();
		try {

			FileInputStream excelFile = new FileInputStream(new File(fileName));
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();

			Customer customer = null;
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				long id = new Double(currentRow.getCell(0).getNumericCellValue()).longValue();
				String email = currentRow.getCell(1).getStringCellValue();
				String customerType = currentRow.getCell(2).getStringCellValue();
				long balance = new Double(currentRow.getCell(3).getNumericCellValue()).longValue();

				customer = new Customer(id,email, customerType, balance);
				customerL.add(customer);

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return customerL;
	}
}
