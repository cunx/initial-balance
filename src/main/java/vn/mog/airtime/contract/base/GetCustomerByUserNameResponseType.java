package vn.mog.airtime.contract.base;

import java.io.Serializable;

import vn.mog.airtime.contract.base.MobiliserResponseType;

public class GetCustomerByUserNameResponseType extends MobiliserResponseType implements Serializable {
	private static final long serialVersionUID = 1L;
	protected Customer customer;

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer value) {
		this.customer = value;
	}
}
