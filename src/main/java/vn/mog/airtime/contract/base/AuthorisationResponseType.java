package vn.mog.airtime.contract.base;

import java.io.Serializable;
import java.util.Date;

public class AuthorisationResponseType extends MobiliserResponseType implements Serializable {
  private static final long serialVersionUID = 1L;

  protected String orderId;

  protected Long txnId;

  protected Date timestamp;

  protected Integer errorCode;

  protected String errorMessage;

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public Long getTxnId() {
    return txnId;
  }

  public void setTxnId(Long txnId) {
    this.txnId = txnId;
  }

  public Date getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  public Integer getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(Integer errorCode) {
    this.errorCode = errorCode;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
}
