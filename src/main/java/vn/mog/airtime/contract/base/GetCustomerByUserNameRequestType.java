package vn.mog.airtime.contract.base;

import java.io.Serializable;

import vn.mog.airtime.contract.base.MobiliserRequestType;

public class GetCustomerByUserNameRequestType extends MobiliserRequestType implements Serializable {
	private static final long serialVersionUID = 1L;

	protected String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
