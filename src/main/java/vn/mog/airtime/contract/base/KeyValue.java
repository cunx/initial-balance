package vn.mog.airtime.contract.base;

import java.io.Serializable;

public class KeyValue implements Serializable {
  private static final long serialVersionUID = 1L;

  protected String key;

  protected String value;

  public String getKey() {
    return this.key;
  }

  public void setKey(String value) {
    this.key = value;
  }

  public String getValue() {
    return this.value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
