package vn.mog.airtime.contract.base;

import java.io.Serializable;

public class AuthorisationRequestType extends MobiliserRequestType implements Serializable {
  private static final long serialVersionUID = 1L;

  protected Integer usecase;

  protected Long payerId;

  protected Long payeeId;

  protected Long amount;

  protected String content;

  protected Boolean autoCapture;

  protected String orderChannel;

  protected String orderId;


  public Long getPayeeId() {
    return payeeId;
  }

  public void setPayeeId(Long payeeId) {
    this.payeeId = payeeId;
  }

  public Long getPayerId() {
    return payerId;
  }

  public void setPayerId(Long payerId) {
    this.payerId = payerId;
  }

  public Long getAmount() {
    return this.amount;
  }

  public void setAmount(Long value) {
    this.amount = value;
  }

  public String getOrderId() {
    return this.orderId;
  }

  public void setOrderId(String value) {
    this.orderId = value;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public boolean isAutoCapture() {
    if (this.autoCapture == null) {
      return false;
    }
    return this.autoCapture.booleanValue();
  }

  public void setAutoCapture(Boolean value) {
    this.autoCapture = value;
  }

  public String getOrderChannel() {
    return this.orderChannel;
  }

  public void setOrderChannel(String value) {
    this.orderChannel = value;
  }

  public Integer getUsecase() {
    return this.usecase;
  }

  public void setUsecase(Integer value) {
    this.usecase = value;
  }
}
