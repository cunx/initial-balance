package vn.mog.airtime.contract.base;

public class Balance {
	private Long currBalance;
	private Long creditBalance;
	private Long creditReservedBalance;
	private Long debitBalance;
	private Long debitReservedBalance;

	public Balance() {
		this.currBalance = 0L;
		this.creditBalance = 0L;
		this.creditReservedBalance = 0L;
		this.debitBalance = 0L;
		this.debitReservedBalance = 0L;
	}

	public Balance(Long currBalance, Long creditBalance, Long creditReservedBalance, Long debitBalance,
			Long debitReservedBalance) {
		this.currBalance = currBalance;
		this.creditBalance = creditBalance;
		this.creditReservedBalance = creditReservedBalance;
		this.debitBalance = debitBalance;
		this.debitReservedBalance = debitReservedBalance;
	}

	public Long getCurrBalance() {
		return currBalance;
	}

	public Long getCreditBalance() {
		return creditBalance;
	}

	public Long getCreditReservedBalance() {
		return creditReservedBalance;
	}

	public Long getDebitBalance() {
		return debitBalance;
	}

	public Long getDebitReservedBalance() {
		return debitReservedBalance;
	}

	public void setCurrBalance(Long currBalance) {
		this.currBalance = currBalance;
	}

	public void setCreditBalance(Long creditBalance) {
		this.creditBalance = creditBalance;
	}

	public void setCreditReservedBalance(Long creditReservedBalance) {
		this.creditReservedBalance = creditReservedBalance;
	}

	public void setDebitBalance(Long debitBalance) {
		this.debitBalance = debitBalance;
	}

	public void setDebitReservedBalance(Long debitReservedBalance) {
		this.debitReservedBalance = debitReservedBalance;
	}

	public void add(Balance balance) {
		if (balance == null)
			return;
		this.currBalance += balance.getCurrBalance();
		this.creditBalance += balance.getCreditBalance();
		this.creditReservedBalance += balance.getCreditReservedBalance();
		this.debitBalance += balance.getDebitBalance();
		this.debitReservedBalance += balance.getDebitReservedBalance();

	}
	
	public Balance clone() {
		Balance balance = new Balance(this.currBalance, this.creditBalance, this.creditReservedBalance, this.debitBalance, this.debitReservedBalance);
		return balance;
	}

}